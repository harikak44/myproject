import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class RupeeTest {
    private Rupee rupee;

    @Test
    public void isValueSameAsAnotherValue() {
        rupee = new Rupee(10);
        assertSame(rupee,rupee);
        assertTrue(rupee == rupee);
    }

    @Test
    void checkIfValuesAreDifferent() {
        assertEquals(new Rupee(10),new Rupee(10));
    }

    @Test
    public void checkIfTenIsNotEqualToNull() {
        assertNotEquals(new Rupee(10), null);
    }

   @Test
    void checkIfTenIsNotEqualToDifferentObject() {
        assertNotEquals(new Rupee(10),
                new Object());
    }

    @Test
    void checkIfTenIsEqualToSevenPlusThree() {
        assertNotEquals(10, new Rupee(7).add(new Rupee(3)));
    }
}

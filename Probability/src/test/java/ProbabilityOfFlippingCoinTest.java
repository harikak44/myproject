import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProbabilityOfFlippingCoinTest {
    private ProbabilityOfFlippingCoin probabilityOfFlippingCoin;

    @Test
    void shouldCheckForInvalidValues() {
        assertThrows(IllegalArgumentException.class, () -> new ProbabilityOfFlippingCoin(0));
    }
    @Test
    public void calculateProbabilityOfGettingAllHeadsOrTailsOnFlippingOneCoin(){
        probabilityOfFlippingCoin = new ProbabilityOfFlippingCoin(1);
        assertEquals(0.5, probabilityOfFlippingCoin.calculateProbabilityOfGettingAllHeadsOrTails());
    }

    @Test
    public void calculateProbabilityOfGettingAllHeadsOrTailsOnFlippingThreeCoins(){
        probabilityOfFlippingCoin = new ProbabilityOfFlippingCoin(4);
        assertEquals(0.0625, probabilityOfFlippingCoin.calculateProbabilityOfGettingAllHeadsOrTails());
    }

    @Test
    public void calculateProbabilityOfGettingAtleastOneHeadOnFlippingThreeCoins(){
        probabilityOfFlippingCoin = new ProbabilityOfFlippingCoin(3);
        assertEquals(0.875, probabilityOfFlippingCoin.calculateProbabilityOfGettingAtleastOneHeadOrTail());
    }

    @Test
    public void calculateProbabilityOfGettingAtmostOneHeadOnFlippingOneCoin() {
        probabilityOfFlippingCoin = new ProbabilityOfFlippingCoin(1);
        assertEquals(0.5, probabilityOfFlippingCoin.calculateProbabilityOfGettingAtmostOneHeadOrTail());
    }

    @Test
    public void calculateProbabilityOfGettingAtmostOneHeadOnFlippingThreeCoins() {
        probabilityOfFlippingCoin = new ProbabilityOfFlippingCoin(3);
        assertEquals(0.375, probabilityOfFlippingCoin.calculateProbabilityOfGettingAtmostOneHeadOrTail());
    }
}

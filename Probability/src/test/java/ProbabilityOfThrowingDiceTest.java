import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProbabilityOfThrowingDiceTest {
    private  ProbabilityOfThrowingDice probabilityOfThrowingDice;

    @Test
    void shouldCheckForInvalidParameters() {
        assertThrows(IllegalArgumentException.class, () -> new ProbabilityOfThrowingDice(0));
    }
    @Test
    public void calculateProbabilityOfThrowingOneDice(){
        probabilityOfThrowingDice = new ProbabilityOfThrowingDice(1);
        assertEquals(0.1666, probabilityOfThrowingDice.calculateProbabilityOfThrowingDice(1));
    }
    @Test
    public void calculateProbabilityOfThrowingTwoDiceAndGettingOne(){
        probabilityOfThrowingDice = new ProbabilityOfThrowingDice(2);
        assertEquals(0, probabilityOfThrowingDice.calculateProbabilityOfThrowingDice(1));
    }

}

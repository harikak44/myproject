public class ProbabilityOfThrowingDice{
    double probability;
    int noOfDice;

    public ProbabilityOfThrowingDice(int noOfDice) {
        if (noOfDice == 0)
            throw new IllegalArgumentException();
        else
            this.noOfDice = noOfDice;

    }

    public double calculateProbabilityOfThrowingDice(int desiredNumber) {
        double sumOfProbability;
        if (noOfDice == 1)
            probability = 0.1666;
        else if (noOfDice > desiredNumber)
            probability = 0;
        else {
            sumOfProbability = 0;
            for (int i = 1; i <= 6; i++) {
                for (int j = 1; j <= 6; j++) {
                    if (i + j == desiredNumber)
                        sumOfProbability = sumOfProbability + calculateProbability(i, noOfDice)
                                + calculateProbability(j, noOfDice);
                    break;
                }
            }
            probability = sumOfProbability;
        }
        return probability;

    }

    private double calculateProbability(int number, int noOfDices) {
        return number / (Math.pow(6, noOfDices));
    }
}

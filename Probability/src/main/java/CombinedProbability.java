public class CombinedProbability {
    ProbabilityOfFlippingCoin probabilityOfFlippingCoin;
    ProbabilityOfThrowingDice probabilityOfThrowingDice;
    double probability;

    public CombinedProbability(int noOfCoins, int noOfDice) {
        probabilityOfFlippingCoin = new ProbabilityOfFlippingCoin(noOfCoins);
        probabilityOfThrowingDice = new ProbabilityOfThrowingDice(noOfDice);
    }

    public double calculateCombinedProbability(int desiredNumber) {
        double probabilityOfGettingHeadOrTail = probabilityOfFlippingCoin.calculateProbabilityOfGettingAtleastOneHeadOrTail();
        double probabilityOfGettingNumberOnDice = probabilityOfThrowingDice.calculateProbabilityOfThrowingDice(desiredNumber);
        probability = (probabilityOfGettingHeadOrTail * probabilityOfGettingNumberOnDice);
        return probability;
    }

    public double calculateProbabilityOEitherfCoinOrDice(int desiredNumber) {
        double probabilityOfGettingHeadOrTail = probabilityOfFlippingCoin.calculateProbabilityOfGettingAtleastOneHeadOrTail();
        double probabilityOfGettingNumberOnDice = probabilityOfThrowingDice.calculateProbabilityOfThrowingDice(desiredNumber);
        probability = probabilityOfGettingHeadOrTail + probabilityOfGettingNumberOnDice;
        return probability;
    }

    public boolean checkIfTwoProbabilitiesAreEqual(int desiredNumber) {
        double probabilityOfGettingHeadOrTail = probabilityOfFlippingCoin.calculateProbabilityOfGettingAtleastOneHeadOrTail();
        double probabilityOfGettingNumberOnDice = probabilityOfThrowingDice.calculateProbabilityOfThrowingDice(desiredNumber);
        return (probabilityOfGettingHeadOrTail == probabilityOfGettingNumberOnDice);
    }
}

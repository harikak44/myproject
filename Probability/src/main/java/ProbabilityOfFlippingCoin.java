
public class ProbabilityOfFlippingCoin {
    int noOfCoins;

    public ProbabilityOfFlippingCoin(int noOfCoins) {
        if (noOfCoins == 0)
            throw new IllegalArgumentException();
        else
            this.noOfCoins = noOfCoins;
    }

    public double calculateProbabilityOfGettingAllHeadsOrTails() {
        return 1 / (Math.pow(2, noOfCoins));
    }

    public double calculateProbabilityOfGettingAtleastOneHeadOrTail() {
        return 1 - (1 / (Math.pow(2, noOfCoins)));
    }

    public double calculateProbabilityOfGettingAtmostOneHeadOrTail() {
        return noOfCoins / (Math.pow(2, noOfCoins));
    }


}

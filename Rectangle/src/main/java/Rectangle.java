public class Rectangle {
    private int length;
    private int breadth;

    public Rectangle(int length, int breadth) throws IllegalArgumentException {
        validateProperties(length, breadth);
        this.length = length;
        this.breadth = breadth;
    }

    private void validateProperties(int length, int breadth) {
        if(length <= 0 || breadth <= 0)
        {
            throw new IllegalArgumentException();
        }
    }

    public int calculateArea() {
        return length * breadth;
    }

    public int calculatePerimeter() {
        return 2 * (length + breadth);
    }

    public static Rectangle CreateSquare(int length) {
        return new Rectangle(length, length);
    }
}

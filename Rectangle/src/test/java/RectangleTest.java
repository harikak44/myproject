import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RectangleTest {
    private Rectangle rectangle;

    @Test
    void shouldCalculateRectangleAreaForLengthFourAndBreadthThree() {
        rectangle = new Rectangle(4,3);
        assertEquals(12, rectangle.calculateArea());
    }

    @Test
    void shouldCalculateRectangleAreaForLengthSevenAndBreadthFive() {
        rectangle = new Rectangle(7,5);
        assertEquals(35, rectangle.calculateArea());
    }

    @Test
    void shouldCheckForInvalidValues() {
        assertThrows(IllegalArgumentException.class, () -> new Rectangle(0, 0));
    }

    @Test
    void shouldCheckForNegativeValues() {
        assertThrows(IllegalArgumentException.class, () -> new Rectangle(-3, -4));
    }

    @Test
    void shouldCalculatePerimeterForLengthTwoAndBreadthTwo(){
        rectangle = new Rectangle(2,2);
        assertEquals(8, rectangle.calculatePerimeter());
    }

    @Test
    void shouldCalculatePerimeterForLengthThreeAndBreadthTwo(){
        rectangle = new Rectangle(3,2);
        assertEquals(10, rectangle.calculatePerimeter());
    }

    @Test
    void shouldCreateSquare(){
        Rectangle rectangle = Rectangle.CreateSquare(4);
        assertEquals(16, rectangle.calculateArea());
        assertEquals(16, rectangle.calculatePerimeter());
    }


}
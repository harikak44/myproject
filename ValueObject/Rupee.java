import java.util.Objects;

public class Rupee {
    private int value;

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rupee that = (Rupee) o;
        return this.value == that.value;
    }

    public Rupee(int value) {
        this.value = value;
    }

    public Rupee add(Rupee rupee){
        return new Rupee(value+rupee.value);
    }


}
